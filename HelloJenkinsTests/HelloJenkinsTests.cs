﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HelloJenkins;

namespace HelloWorldTests
{
    [TestClass]
    public class HelloJenkinsTests
    {
        [TestMethod]
        public void OutputIsHelloJenkins()
        {
            Assert.AreEqual("Hello Jenkins!", HelloJenkins.Program.CreateMessage());
        }
    }
}
